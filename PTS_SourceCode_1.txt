@RestResource(urlMapping='/api/webhooks/created/*')

global with sharing class CreateRecord {
    @HttpGet
    global static String getIssueKey(){
        RestRequest req = RestContext.request;
        RestRequest request = RestContext.request;
        String issueKey = request.requestURI.substring(request.requestURI.lastIndexOf('/')+1);
        return issueKey;
    }

    global static Map<String,String> getIssueDetails(){
        
        Map<String, String> issueDetails = new Map<String, String>();
        
        HttpRequest req = new HttpRequest();
        String issueKey = getIssueKey();
        issueDetails.put('key',issueKey);
        String endPoint = 'https://b-ackerman.atlassian.net/rest/api/3/issue/'+issueKey;
        String name = 'bkumari@crgroup.co.in';
        String password = 'yeb2i6dxmhckPE1CI78l9C92';
        
        Blob headerValue = Blob.valueOf(name +':'+ password);
    	String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader); 
        req.setEndpoint(endPoint);
        req.setMethod('GET');
        
        Http http = new Http();
    	HTTPResponse res = http.send(req);
     	System.debug(res.getBody());
        System.debug(res.getStatusCode());
        
        JSONParser parser = JSON.createParser(res.getBody());
        while (parser.nextToken() != null) {
            if(parser.getCurrentName()=='summary'){
                parser.nextToken();
                System.debug('Summary:'+parser.getText());
                issueDetails.put(parser.getCurrentName(),parser.getText());
                
            } else if (parser.getCurrentName()=='status'){
                if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                    Status s = (Status)parser.readValueAs(Status.class);
                    System.debug('Status object: ' + s.name);
                    issueDetails.put(parser.getCurrentName(),s.name);
                }        
            }   
        }
        System.debug(issueDetails);
        return issueDetails;
        
    }


    @HttpPost
    global static void CreateJiraRecord() {
        Map<String, String> issueDetails = getIssueDetails();
    	Jira__C j = new Jira__C(Name=issueDetails.get('summary'),Issue_Key__c=issueDetails.get('key'),Status__c=issueDetails.get('status'));
    	insert j;     
    }
    global class Status{
        public String self ;
    	public String description;
    	public String iconUrl ;
        public String name;
        public String id;
        
        public Status(String self,String description,String iconUrl,String name,String id) {
        	self = self;
            description = description;
            iconUrl = iconUrl;
            name = name;
            id =id;
    	}
    } 
}
